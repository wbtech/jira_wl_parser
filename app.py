from collections import defaultdict

import requests
from flask import Flask
from flask.json import jsonify
from requests.auth import HTTPBasicAuth


USERNAME = 'jira@wbtech.pro'
API_TOKEN = 'rwcX1dkvzjcdKONaYepdA0A9'


def get_issue_worklogs(issue):
    url = f'https://wbtech.atlassian.net/rest/api/3/issue/{issue}/worklog'
    auth = HTTPBasicAuth(USERNAME, API_TOKEN)
    headers = {"Accept": "application/json"}
    response = requests.request("GET", url, headers=headers, auth=auth)
    return response.json()


def get_child_issues_keys(issue) -> list:
    url = f'https://wbtech.atlassian.net/rest/api/3/search'
    auth = HTTPBasicAuth(USERNAME, API_TOKEN)
    params = {'jql': f'"Parent Link"={issue}'}
    headers = {"Accept": "application/json"}
    response = requests.request("GET", url, headers=headers, auth=auth, params=params)
    issues = response.json().get('issues')
    ret_list = []
    if issues:
        ret_list.extend([i['key'] for i in issues])
    return ret_list


def recursively_search_children(issues_to_find=[], return_list=[]):
    """ Mutate return list """
    for issue_key in issues_to_find:
        child_keys = get_child_issues_keys(issue_key)
        return_list.extend(child_keys)
        if child_keys:
            recursively_search_children(child_keys, return_list)


def get_all_unique_issues_in_tree(issues_key):
    issues_to_find = [issues_key]
    issues_children_list = []
    recursively_search_children(issues_to_find, issues_children_list)
    issues_to_find.extend(issues_children_list)
    return list(set(issues_to_find))


def get_worklogs_for_issues_list(issues_list):
    all_worklogs = []
    for issues_key in issues_list:
        issue_worklogs = get_issue_worklogs(issues_key)
        all_worklogs.extend(issue_worklogs['worklogs'])
    return all_worklogs


def get_worklog_merged_by_author(issue_key):
    unique_isuues_keys = get_all_unique_issues_in_tree(issue_key)
    worklogs = get_worklogs_for_issues_list(unique_isuues_keys)

    worklogs_by_author = defaultdict(list)
    names = defaultdict(set)
    for worklog in worklogs:
        worklogs_by_author[worklog['author']['accountId']].append(worklog['timeSpentSeconds'])
        names[worklog['author']['accountId']].add(worklog['author']['displayName'])

    worklogs_by_author_summed = dict()
    for key, value in worklogs_by_author.items():
        worklogs_by_author_summed[key] = sum(value)

    return_array = []
    for key, value in worklogs_by_author_summed.items():
        item = dict()
        item['accountId'] = key
        item['time'] = value
        item['displayName'] = list(names[key])[0]
        return_array.append(item)

    return return_array


app = Flask(__name__)


@app.route("/jira-task-parser/<issue_key>", methods=['GET'])
def index(issue_key):
    task_index = str(issue_key).upper()

    return jsonify(get_worklog_merged_by_author(task_index))

'''
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=4567)
'''
